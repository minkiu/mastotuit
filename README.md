# mastotuit
Publish automagically to Twitter all your Mastodon posts!

### Dependencies

-   **Python 3**
-   Postgresql server
-   Mastodon user account  
-   Your personal Linux driven PC or laptop

### Usage:

Within Python Virtual Environment:

1. Run `pip install -r requirements.txt` to install needed libraries.

2. Run `python db-setup.py` to setup and create new Postgresql database and needed table in it and setup your Mastodon's account RSS feed in the format 'https://your.mastodon.server/@your_user.rss'

3. Run `python setup.py` to input and save your Twitter's key and access tokens. You can get your keys and tokens from [Twitter Developer Platform](https://developer.twitter.com/en/apply/user.html)  

4. Run `python mastodon-setup.py` to setup your Mastodon account access tokens.

5. Use your favourite scheduling method to set `python mastotuit.py` to run every minute.  

29.9.2021 **New Feature** Added support to media files! mastotuit now gets all media files from Mastodon's post (if any) and publish them to Twitter together with your status update.  
7.10.2021 **New Feature** Added thread support! If you create a thread in Mastodon mastotuit will create the same thread on Twitter.  
13.10.2021 Upgraded Tweepy library to v4.1.0  
13.10.2021 **New Feature** Added video upload support! If video properties are according Twitter rules it will be uploaded.
