wheel>=0.37.0
psycopg2>=2.9.1
feedparser>=6.0.8
bs4>=0.0.1
Mastodon.py>=1.5.1
tweepy==4.1.0
filetype>=1.0.8
ffmpeg-python>=0.2.0
